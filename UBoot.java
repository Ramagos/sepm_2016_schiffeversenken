public class UBoot extends Schiff {
	private int x1;
	private int y1;
	
	protected UBoot(int x1, int y1, Richtung.Orientierung o){
		super(x1,y1,o,4);
		this.x1=x1;
		this.y1=y1;
	}
	protected UBoot(int index, Richtung.Orientierung o, int l){
		super(index,o,4);
		this.x1=Punkt.IndexToX(index);
		this.y1=Punkt.IndexToY(index);
		this.laenge=l;
	}
}
