import java.util.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;

public class GameClass extends JFrame implements ActionListener{
	public static int anzahlTanker;
	public static int anzahlSpeedboote;
	public static int anzahlUBoot;
	public static int sizex;
	public static int sizey;
	public static int anzahlSchiffe;
	
	ArrayList <Schiff> schiffeSpieler1=new ArrayList <Schiff>();
	ArrayList <Schiff> schiffeSpieler2=new ArrayList <Schiff>();
	ArrayList <Punkt> spieler1Schüsse=new ArrayList <Punkt>();
	ArrayList <Punkt> spieler2Schüsse=new ArrayList <Punkt>();
	ArrayList <JButton> knopfeLinks=new ArrayList<JButton>();
	ArrayList <JButton> knopfeRechts=new ArrayList<JButton>();

	boolean[] feld;
	int durchlaufv=1;
	public static Spielstatus status;

	JButton lastButton1=new JButton();

	int aktuelleLange;
	Richtung.Orientierung aktuelleOrientierung;

	public GameClass(int at, int as, int au, int sizex, int sizey){
		this.anzahlTanker=at;
		this.anzahlSpeedboote=as;
		this.anzahlUBoot=au;
		this.sizex=sizex;
		this.sizey=sizey;
		anzahlSchiffe=as+at+au;
		feld=new boolean[sizex*sizey];
		for(int i=0;i<=feld.length-1;i++){
			feld[i]=false;
		}
		status=Spielstatus.setzen;
	}

	JPanel place=new JPanel();
	JPanel panel = new JPanel();	
	JPanel panel2=new JPanel();
	JButton set=new JButton();
	ActionListener q=new seter();

	public void DrawPlayingField(){
		this.setTitle("Schiffeversenken");
		panel.setPreferredSize(new Dimension(500, 500));
		panel.setLayout(new GridLayout(sizex+1,sizey+2,2,2));
		panel.setBorder(new LineBorder(Color.WHITE, 10));
		panel.setBackground(Color.BLACK);

		panel2.setPreferredSize(new Dimension(500, 500));
		panel2.setLayout(new GridLayout(sizex+1,sizey+2,2,2));
		panel2.setBorder(new LineBorder(Color.WHITE, 10));
		panel2.setBackground(Color.BLACK);

		JButton b=new JButton();
		b.setBackground(Color.BLACK);
		panel.add(b);

		for(int i=0;i<sizex;i++){
			b=new JButton();
			b.setBackground(Color.BLUE);
			b.setText(Integer.toString(i+1));
			panel.add(b);
		}

		for(int i=0;i<10;i++){
			for(int j=0;j<=10;j++){
				if(j==0){
					b=new JButton();
					b.setBackground(Color.BLUE);
					b.setText(Integer.toString(i+1));
					panel.add(b);
				}else{
					b=new JButton();
					b.setBackground(Color.GRAY);
					b.addActionListener(this);
					panel.add(b);
					knopfeLinks.add(b);
				}
			}
		}
		this.add(panel2, BorderLayout.EAST);
		this.add(panel,BorderLayout.WEST);

		if(status==Spielstatus.setzen){
			JButton Spd=new JButton();
			ActionListener n=new Spdboot();
			Spd.addActionListener(n);
			Spd.setText("Speedboot");
			place.add(Spd,BorderLayout.SOUTH);

			JButton Tank=new JButton();
			ActionListener o=new Tank();
			Tank.addActionListener(o);
			Tank.setText("Tanker");
			place.add(Tank,BorderLayout.SOUTH);

			JButton Ub=new JButton();
			ActionListener p=new Uboot();
			Ub.addActionListener(p);
			Ub.setText("U-boot");
			place.add(Ub,BorderLayout.SOUTH);

			JButton bn=new JButton();
			ActionListener l=new No();
			bn.addActionListener(l);
			bn.setText("Norden");
			place.add(bn,BorderLayout.SOUTH);

			JButton bo=new JButton();
			bo.setText("Osten");
			ActionListener m=new Os();
			bo.addActionListener(m);
			place.add(bo,BorderLayout.SOUTH);

			set.setText("Setzen");
			set.addActionListener(q);
			place.add(set,BorderLayout.SOUTH);

			this.add(place, BorderLayout.SOUTH);
		}
		this.pack();
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);		
		DrawPlayingField1();
	}

	JTextField ausgabe=new JTextField();
	public void DrawPlayingField2(){
		JPanel playplace=new JPanel();
		if(status==Spielstatus.spielen){
			this.remove(place);
			JButton schuss=new JButton();
			ActionListener r=new schuss();
			schuss.addActionListener(r);
			schuss.setText("Schuss");
			playplace.add(schuss,BorderLayout.SOUTH);

			ausgabe.setColumns(20);
			playplace.add(ausgabe,BorderLayout.SOUTH);

			this.add(playplace, BorderLayout.SOUTH);
		}
		for(int i=0;i<knopfeLinks.size();i++){
			ActionListener s=new schussk();
			knopfeLinks.get(i).removeActionListener(this);
			knopfeLinks.get(i).addActionListener(s);
		}
		this.pack();
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public void DrawPlayingField1(){
		JButton b=new JButton();
		b.setBackground(Color.BLACK);
		panel2.add(b);

		for(int i=0;i<sizex;i++){
			b=new JButton();
			b.setBackground(Color.BLUE);
			b.setText(Integer.toString(i+1));
			panel2.add(b);
		}

		for(int i=0;i<10;i++){
			for(int j=0;j<=10;j++){
				if(j==0){
					b=new JButton();
					b.setBackground(Color.BLUE);
					b.setText(Integer.toString(i+1));
					panel2.add(b);
				}else{
					b=new JButton();
					b.setBackground(Color.GRAY);
					b.addActionListener(this);
					panel2.add(b);
					knopfeRechts.add(b);
				}				
			}
		}
		set.removeActionListener(q);
		q=new seter2();
		set.addActionListener(q);
		this.add(panel2, BorderLayout.EAST);

	}

	public void StatusAuswerten(){

	}
	enum Spielstatus{
		setzen,
		spielen,
		beendet,
	}
	public void actionPerformed(ActionEvent e) {
		((JButton) e.getSource()).setBackground(Color.RED);
		if(lastButton1!=null)lastButton1.setBackground(Color.GRAY);
		lastButton1=(JButton) e.getSource();
	}
	class No implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int Index=knopfeLinks.indexOf(lastButton1);
			JButton knopf;
			for (int i = 0; i < aktuelleLange; i++) {
				knopf=knopfeLinks.get(Index-i*sizex);
				knopf.setBackground(Color.RED);
			}
			aktuelleOrientierung=Richtung.Orientierung.N;
		}
	}
	class Os implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int Index=knopfeLinks.indexOf(lastButton1);
			JButton knopf;
			for (int i = 0; i < aktuelleLange; i++) {
				knopf=knopfeLinks.get(Index+i);
				knopf.setBackground(Color.RED);
			}
			aktuelleOrientierung=Richtung.Orientierung.O;
		}
	}
	class Spdboot implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			aktuelleLange=2;
		}
	}
	class Uboot implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			aktuelleLange=4;
		}
	}
	class Tank implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			aktuelleLange=3;
		}
	}
	class seter implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			if(aktuelleLange==2){
				anzahlSpeedboote--;
				schiffeSpieler1.add(new SpeedBoat(((int)knopfeLinks.indexOf(lastButton1)), aktuelleOrientierung, aktuelleLange));
			}
			if(aktuelleLange==3){
				anzahlTanker--;
				schiffeSpieler1.add(new UBoot(((int)knopfeLinks.indexOf(lastButton1)), aktuelleOrientierung, aktuelleLange));
			}
			if(aktuelleLange==4){
				anzahlUBoot--;
				schiffeSpieler1.add(new Tanker(((int)knopfeLinks.indexOf(lastButton1)), aktuelleOrientierung, aktuelleLange));
			}
			lastButton1=null;
			if((anzahlSpeedboote<=0)&&(anzahlTanker<=0)&&(anzahlUBoot<=0)){
				status=Spielstatus.spielen;
				DrawPlayingField1();
			}
		}
	}
	class seter2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {	
			if(aktuelleLange==2){
				anzahlSpeedboote--;
				schiffeSpieler1.add(new SpeedBoat(((int)knopfeLinks.indexOf(lastButton1)), aktuelleOrientierung, aktuelleLange));
			}
			if(aktuelleLange==3){
				anzahlTanker--;
				schiffeSpieler1.add(new Tanker(((int)knopfeLinks.indexOf(lastButton1)), aktuelleOrientierung, aktuelleLange));
			}
			if(aktuelleLange==4){
				anzahlUBoot--;
				schiffeSpieler1.add(new UBoot(((int)knopfeLinks.indexOf(lastButton1)), aktuelleOrientierung, aktuelleLange));
			}
			lastButton1=null;
			if((anzahlSpeedboote<=0)&&(anzahlTanker<=0)&&(anzahlUBoot<=0)){
				status=Spielstatus.spielen;
				DrawPlayingField2();
			}
		}
	}

	Color knopffarbe;
	class schussk implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			ausgabe.setText("");
			if(lastButton1!=null)lastButton1.setBackground(knopffarbe);			
			knopffarbe=((JButton) e.getSource()).getBackground();				
			((JButton) e.getSource()).setBackground(Color.YELLOW);
			lastButton1=(JButton) e.getSource();
		}
	}
	static class abrunden{
		public static int rund(double r){
			if((r>=0)&&(r<1))return 0;
			if((r>=1)&&(r<2))return 1;
			if((r>=2)&&(r<3))return 2;
			if((r>=3)&&(r<4))return 3;
			if((r>=4)&&(r<5))return 4;
			if((r>=5)&&(r<6))return 5;
			if((r>=6)&&(r<7))return 6;
			if((r>=7)&&(r<8))return 7;
			if((r>=8)&&(r<9))return 8;
			if((r>=9)&&(r<10))return 9;
			return 0;
		}
	}
	int anzahlZerstoert=0;
	class schuss implements ActionListener {
		int zerstörteVorigerDurchlauf=0;
		public void actionPerformed(ActionEvent e) {
			anzahlZerstoert=0;
			int index=knopfeLinks.indexOf(lastButton1);
			Punkt point=Punkt.IndexToPunkt(index);
			spieler1Schüsse.add(point);
			
			if(knopffarbe==Color.RED){
				ausgabe.setText("Schiff getroffen");
				for(int i=0;i<schiffeSpieler1.size();i++){
					schiffeSpieler1.get(i).istGetroffen(point);
					if(schiffeSpieler1.get(i).istZerstort()){
						anzahlZerstoert++;
					}
				}
			}
			else ausgabe.setText("Schiff verfehlt");
			if((anzahlZerstoert!=zerstörteVorigerDurchlauf)){						//FEHLER
				ausgabe.setText("Schiff zerstört");
				zerstörteVorigerDurchlauf=anzahlZerstoert;
			}
			if(anzahlZerstoert==anzahlSchiffe){
				status=Spielstatus.beendet;
				beendet();
			}
		}
	}
	private File datei;
	private AudioClip clip;
	public void beendet(){
		datei = new File ("Guiles_theme_download.wav");
		try{
			clip = Applet.newAudioClip(datei.toURL());
			clip.play();
			Thread.sleep(7600);
			clip.stop();
		}
		catch(Exception e){
			System.err.println("Error: "+e.getMessage());
		}
	}
}