import java.util.*;

import javax.swing.*;

public abstract class Schiff {
	public Punkt ursprung;
	public String richtung;
	public Richtung.Orientierung orientierung;
	public ArrayList<Punkt> getroffenePunkte=new ArrayList <Punkt>();
	public ArrayList<Punkt> punkteSchiff=new ArrayList <Punkt>();
	int laenge;

	public Schiff (int x1, int y1, Richtung.Orientierung orientierung, int laenge){
		this.setUrsprung(x1-1,y1);
		this.orientierung=orientierung;
	}
	public Schiff (int index, Richtung.Orientierung orientierung, int laenge){
		this.setUrsprung(index-Punkt.IndexToX(index),Punkt.IndexToY(index));
		this.punkteSchiff.add(new Punkt(index-Punkt.IndexToX(index),Punkt.IndexToY(index)));

		if(orientierung==Richtung.Orientierung.N){
			for(int i=1;i<=laenge;i++)
				this.punkteSchiff.add(new Punkt(index-Punkt.IndexToX(index),Punkt.IndexToY(index)-GameClass.sizex*i));
		}
		if(orientierung==Richtung.Orientierung.O){
			for(int i=1;i<=laenge;i++)
				this.punkteSchiff.add(new Punkt(Punkt.IndexToX(index)+i,Punkt.IndexToY(index)));
		}

		this.orientierung=orientierung;
		this.laenge=laenge;
	}

	public Punkt getUrsprung(){
		return ursprung;
	}
	
	public int getLaenge(){
		return this.laenge;
	}
	
	private void setUrsprung(int x, int y){
		ursprung=new Punkt(x,y);
	}

	public void Erkl�rung(){
		System.out.println("Ich bin ein "+this.getClass().toString().substring(6)+" stehe auf den Kordinaten "+this.getUrsprung().toString()+" und bin "+this.getLaenge()+" Felder lang");
	}
	public String toString(){
		return(this.getClass().toString().substring(6)+" auf "+this.getUrsprung().toString()+" mit l= "+this.getLaenge());
	}
	
	public void istGetroffen(Punkt p){
		if( punkteSchiff.contains(p)) {
			getroffenePunkte.add(p);
			punkteSchiff.remove(p);
			//System.out.println(this.toString() + " wurde getroffen");
		}
	}

	public boolean istZerstort(){
		//System.out.println(this.toString() + " wird gepr�ft");
		if(this.getroffenePunkte.size()==this.getLaenge()){
			//System.out.println(this.toString()+ " wurde zerst�rt");
			return true;
		}
		return false;
	}
}